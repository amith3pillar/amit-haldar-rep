package com.pp.base;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import com.pp.comman.Menu;
import com.pp.utils.Constants;
import com.pp.utils.ExtentManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BasePage {
	
	public ExtentReports extent=ExtentManager.getInstance();
	public ExtentTest test;
	public WebDriver driver;
	public Menu menu;
	
	public BasePage(){}
	
	 public BasePage(WebDriver driver, ExtentTest test){
		this.driver=driver;
		this.test=test;
		menu=PageFactory.initElements(driver, Menu.class);
	}
	
	
public void init(String browserType){
		if(browserType.equalsIgnoreCase("Mozilla")){
			System.setProperty("webdriver.gecko.driver",Constants.Mozilla_DRIVER_EXE );
			driver= new FirefoxDriver();
		}else if (browserType.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver",Constants.CHROME_DRIVER_EXE);
			//driver= new ChromeDriver();
			
			DesiredCapabilities ieCapabilities = DesiredCapabilities.chrome();
			  ChromeOptions options = new ChromeOptions();
			     options.addArguments("disable-infobars");
			     options.addArguments("--start-maximized");
			     options.addArguments("--disable-web-security");
			     options.addArguments("--no-proxy-server");
			     Map<String, Object> prefs = new HashMap<String, Object>();
			     prefs.put("credentials_enable_service", false);
			     prefs.put("profile.password_manager_enabled", false);
			     options.setExperimentalOption("prefs", prefs);
			     driver= new ChromeDriver(options);
			
		}else if (browserType.equalsIgnoreCase("Internet Explrer")){
			System.setProperty("webdriver.ie.driver",Constants.Internet_DRIVER_EXE);
			driver= new InternetExplorerDriver();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	
	public Menu  getMenu(){
		return menu;
	}
	
	public void takeScreenShot(){
		Date d= new Date();
		String screenShotFile=d.toString().replace(":","_").replace("","_")+".png";
		String filePath=Constants.REPORTS_PATH+ "//ScreenShots//"+screenShotFile;
		//Store screenshot in that file
		File scrFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try{
			FileUtils.copyFile(scrFile,new File(filePath));
		}catch(IOException e){
			e.printStackTrace();
		}
		test.log(LogStatus.INFO, test.addScreenCapture(filePath));
		
		test.log(LogStatus.INFO, test.addScreenCapture(""));
	}
}
