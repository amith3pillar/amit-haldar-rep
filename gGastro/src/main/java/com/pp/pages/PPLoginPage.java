package com.pp.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.pp.OR.PPLoginPageOR;
import com.pp.base.BasePage;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class PPLoginPage extends BasePage{

	
	@FindBy(xpath=PPLoginPageOR.LoginUserName)
	public WebElement username;
	
	@FindBy(xpath=PPLoginPageOR.LoginPassWord)
	public WebElement password;
	
	@FindBy(xpath=PPLoginPageOR.LoginSubmitBtn)
	public WebElement LSubmitBtn;
	

	
public PPLoginPage(WebDriver driver, ExtentTest test){
	super (driver,test);
		
	}
	

	public LandingPage doLogin(String usename,String Password){
		//test.log(LogStatus.INFO, "Enter UserName and Password");
		username.sendKeys(usename);
		password.sendKeys(Password);
		LSubmitBtn.click();
		
		LandingPage landingpage = new LandingPage(driver,test);
		PageFactory.initElements(driver, landingpage);
		return landingpage;
		
		
		//return PageFactory.initElements(driver, LandingPage.class);
	
	}
}
