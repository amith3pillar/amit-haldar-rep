package com.pp.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.pp.base.BasePage;
import com.pp.utils.Constants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LaunchPage extends BasePage {
	


	public  LaunchPage(WebDriver driver,ExtentTest test){
		super(driver,test);
		
		
	}
	
	
	public PPLoginPage gotoPPLoginPage(){
		test.log(LogStatus.INFO, "Opening the URL" + Constants.PATIENTPORTAL_URL);
		driver.get(Constants.PATIENTPORTAL_URL);
		test.log(LogStatus.PASS, "URL Opened" + Constants.PATIENTPORTAL_URL);
		
		PPLoginPage pploginpage = new PPLoginPage(driver,test);
		PageFactory.initElements(driver, pploginpage);
		return pploginpage;
		
		//return PageFactory.initElements(driver, PPLoginPage.class);
	
	}

}
