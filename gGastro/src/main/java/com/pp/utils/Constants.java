package com.pp.utils;

public class Constants {
	
	public static final String CHROME_DRIVER_EXE="D:\\Software\\Drivers_Exe\\chromedriver.exe";
	public static final String Mozilla_DRIVER_EXE="D:\\Software\\Drivers_Exe\\geckodriver.exe";
	public static final String Internet_DRIVER_EXE="D:\\Software\\Drivers_Exe\\IEDriverServer.exe";
	public static final String PATIENTPORTAL_URL = "http://gmedbuild01/portal-enhance/Account/LogOn";
	public static final String REPORTS_PATH = "D:\\Selenium\\Workspace\\PageFactoryPP\\Reports\\";
}
