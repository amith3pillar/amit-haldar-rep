package com.pp.comman;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.pp.OR.MenuOR;
import com.pp.base.BasePage;

public class Menu extends BasePage{
	


	@FindBy(xpath=MenuOR.LOGOUT_BUTTON)
	public WebElement lougoutBtn;
	
	WebDriver driver;
	
	public  Menu(WebDriver driver){
		this.driver=driver;
		
	}

	
	
	
	public void logout(){
		
		lougoutBtn.click();
		
	}
}
