package com.pp.testcases;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.pp.base.BasePage;
import com.pp.pages.LaunchPage;
import com.pp.pages.PPLoginPage;

import com.relevantcodes.extentreports.LogStatus;



public class ProfileTest extends BasePage{
   
	
	
	@Test
	public void doLogin() throws InterruptedException{
		
	
	test= extent.startTest("Login Test");
	test.log(LogStatus.INFO, "Staring login Test");
	test.log(LogStatus.INFO, "Opening Browser");
	
	init("chrome");
		LaunchPage lauchpage= new LaunchPage(driver,test);
		PageFactory.initElements(driver, lauchpage);
		
		//LaunchPage lauchpage= PageFactory.initElements(driver, LaunchPage.class);
		
		PPLoginPage pploginpage= lauchpage.gotoPPLoginPage();
		pploginpage.doLogin("NPJ30", "a@123");
		test.log(LogStatus.INFO, "Login into Patient Portal");
		Thread.sleep(5000);
		pploginpage.takeScreenShot();
		pploginpage.menu.logout();
		test.log(LogStatus.INFO, "Logout from Patient Portal");
		
		
	
		
	}
	@AfterMethod
	public void quit(){
		if (extent!=null){
			extent.endTest(test);
			extent.flush();
		}
		
	}
			
}
